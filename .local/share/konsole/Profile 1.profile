[Appearance]
BoldIntense=false
ColorScheme=Breeze

[General]
InvertSelectionColors=true
Name=Profile 1
Parent=FALLBACK/

[Interaction Options]
AllowEscapedLinks=false
CopyTextAsHTML=false
CtrlRequiredForDrag=false
MouseWheelZoomEnabled=false
OpenLinksByDirectClickEnabled=true
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2
