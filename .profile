[ -n "$BASH_VERSION" ] && \
    [ -f "$HOME/.bashrc" ] && \
		. "$HOME/.bashrc"

[ -d "$HOME/bin" ] && echo "$PATH" | grep -q "$HOME/bin" - || \
    export PATH="$HOME/bin:$PATH"

export GIT_SSH_COMMAND="ssh -i /home/adomino/.ssh/adominodsq"
export NVM_DIR="$HOME/.nvm"
[ -n "$BASH_VERSION" ] && \
	[ -s "$NVM_DIR/nvm.sh" ] && \
		. "$NVM_DIR/nvm.sh"
