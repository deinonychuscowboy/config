# Heavily modified form of Ubuntu's default bashrc, containing mostly the
# "automagic prompt" I use in bash, which only displays when needed
# (e.g., when you change directories, the statusline will show, when you
# execute several commands in the same directory, the statusline will stay
# hidden.)

# You can also set it to display the directory contents and git status,
# I no longer use these

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

#!/bin/bash
# Automagic prompt
#
# In verbose mode, brief prompt includes: statusline, git ps1
#                  full  prompt adds    : ls, git status
# In quiet   mode, brief prompt includes: statusline
#                  full  prompt adds    : git ps1
#
# If one of the referenced values changes, it is included in the next
# prompt, up to the full prompt. If nothing has changed, the prompt is
# a simple $ or #.
# You can explicitly show a set of prompt elements using pb and pf. If
# the prompt is being too chatty (e.g. in directories with many items or
# large git repositories), you can use pq to simplify the output.
#
# user commands: pf (prompt full)
PS1_QUIET=true

git_ps1()
{
    echo -ne "\033[01;33m"
    branch="$(git branch 2>&1 | grep "^*" | sed "s/^* //")"
    printf " $branch"
    echo -ne "\033[00m"

    if [ -n "$branch" ]; then
        printf " "
        gitvals=""
        for x in $(git status -s 2>&1); do
            val="$(echo $x | cut -d" " -f1 | sed 's/^\(.\).*/\1/')"
            if echo "$gitvals" | grep -q "$val"; then
                :
            elif echo "MAD?TRC" | grep -q "$val"; then
                gitvals="$gitvals\n$val"
            fi
        done
        if [ -n "$gitvals" ]; then
            for x in "$(echo -e $gitvals | sort)"; do
                x="${x#"${x%%[![:space:]]*}"}"
                x="${x%"${x##*[![:space:]]}"}"
                if [ "$x" = "A" ]; then
                    echo -ne "\033[00;92m"
                    printf "+"
                    echo -ne "\033[00m"
                elif [ "$x" = "D" ]; then
                    echo -ne "\033[00;91m"
                    printf "-"
                    echo -ne "\033[00m"
                elif [ "$x" = "?" ]; then
                    echo -ne "\033[00;95m"
                    printf "?"
                    echo -ne "\033[00m"
                elif [ "$x" = "M" ]; then
                    echo -ne "\033[00;94m"
                    printf "*"
                    echo -ne "\033[00m"
                elif [ "$x" = "T" ]; then
                    echo -ne "\033[00;94m"
                    printf ":"
                    echo -ne "\033[00m"
                elif [ "$x" = "R" ]; then
                    echo -ne "\033[00;93m"
                    printf ">"
                    echo -ne "\033[00m"
                elif [ "$x" = "C" ]; then
                    echo -ne "\033[00;96m"
                    printf "%"
                    echo -ne "\033[00m"
                fi
            done
        fi
    fi
}

set_ps1()
{
    last_status=$?
    who="${USER}"
    PS1_PWD_NEW=$PWD
    PS1_GIT_NEW="$(git_ps1)"
    PS1_NEW="${debian_chroot:+[$debian_chroot] }\[\033[0;32m\]$(printf '%q' $who)\[\033[01;30m\]@\[\033[01;34m\]$(uname -n)\[\033[01;30m\]:\[\033[0m\]\[\033[01m\]${PS1_PWD_NEW}\[\033[01;33m\]"
    PS1=""
    if [ "$PS1_NEW" != "$PS1_OLD" -o "$PS1_GIT_OLD" != "$PS1_GIT_NEW" ]; then
        PS1+="$PS1_NEW"
        if [ $PS1_QUIET != true -o "$PS1_GIT_OLD" != "$PS1_GIT_NEW" ]; then
			PS1+="${PS1_GIT_NEW}"
		fi
		PS1+="\[\033[0m\]"
        PS1_LAST=true
    else
        PS1_LAST=false
    fi
    char="\$"
    if [ "$who" == "root" ]; then
        char="#"
    fi
    if [ $last_status -eq 0 ]; then
        char="\[\033[00;01m\]$char"
    else
        char="\[\033[01;31m\]$char"
    fi
    PS1+="\n$char\[\033[0m\] "
    PS1_OLD="$PS1_NEW"
    PS1_PWD_OLD="$PS1_PWD_NEW"
    PS1_GIT_OLD="$PS1_GIT_NEW"
}
ps1_cmd()
{
    if [ $PS1_QUIET != true ]; then
        if [ $PS1_LAST = true ]; then
            echo -ne "\n"
        fi
    elif [ $PS1_LAST = true ]; then
		echo -ne "\n"
	fi
}
pf()
{
    PS1_OLD=""
    PS1_PWD_OLD=""
    PS1_GIT_OLD=""
    PS1_LAST=false
}
PROMPT_COMMAND="set_ps1;ps1_cmd"

# set variable identifying the chroot you work in
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+[$debian_chroot] }\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

alias lha='ls -lha'
alias df="df -h"
alias du="du -h --apparent-size"
alias pkexec='pkexec env DISPLAY="$DISPLAY" XAUTHORITY="$XAUTHORITY" DBUS_SESSION_BUS_ADDRESS="$DBUS_SESSION_BUS_ADDRESS"'
alias nano='nano -AWcimwxlT4'
alias gitk='gitk --all --date-order'
alias clip='xclip -selection clipboard'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -f "$HOME/.autosudorc" ]; then
	. "$HOME/.autosudorc"
fi

