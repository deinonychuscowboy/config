# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ;
    export PATH="$HOME/bin:$PATH"
end

set fishpromptvar ""

# two-line prompt that collapses to 1 line if the first line has not changed
# generate prompt
function generate_fish_prompt
    if [ -r /etc/debian_chroot ];
        printf "[" (cat /etc/debian_chroot) "]"
    end
    set who (whoami)
    if [ $who = "root" ]
        echo -ne "\033[01;31m"
    else
        echo -ne "\033[00;32m"
    end
    printf "%s" $who
    echo -ne "\033[01;30m"
    printf "@"
    echo -ne "\033[01;34m"
    printf "%s" (uname -n)
    echo -ne "\033[01;30m"
    printf ":"
    echo -ne "\033[0m"
    echo -ne "\033[01m"
    printf "%s" (pwd)
    echo -ne "\033[01;33m"
    set branch (git branch 2>&1 | grep "^*" | sed "s/^* //")
    printf " $branch"
    echo -ne "\033[00m"

    if [ -n "$branch" ]
        printf " "
        set gitvals ""
        for x in (git status -s 2>&1)
            set val (echo $x | cut -d" " -f1 | sed 's/^\(.\).*/\1/')
            if echo "$gitvals" | grep -q "$val"
                :
            else if echo "MAD?TRC" | grep -q "$val"
                set gitvals "$gitvals\n$val"
            end
        end
        if [ -n "$gitvals" ]
            for x in (echo -e $gitvals | sort);
                if [ "$x" = "A" ];
                    echo -ne "\033[00;92m"
                    printf "+"
                    echo -ne "\033[00m"
                else if [ "$x" = "D" ];
                    echo -ne "\033[00;91m"
                    printf "-"
                    echo -ne "\033[00m"
                else if [ "$x" = "?" ];
                    echo -ne "\033[00;95m"
                    printf "?"
                    echo -ne "\033[00m"
                else if [ "$x" = "M" ];
                    echo -ne "\033[00;94m"
                    printf "*"
                    echo -ne "\033[00m"
                else if [ "$x" = "T" ];
                    echo -ne "\033[00;94m"
                    printf ":"
                    echo -ne "\033[00m"
                else if [ "$x" = "R" ];
                    echo -ne "\033[00;93m"
                    printf ">"
                    echo -ne "\033[00m"
                else if [ "$x" = "C" ];
                    echo -ne "\033[00;96m"
                    printf "%"
                    echo -ne "\033[00m"
                end
            end
        end
    end
end

# show the prompt
function fish_prompt
    set -l last_status $status
    # comment out next line if you don't want a blank line between commands
    printf "\n"
    set tempfish (generate_fish_prompt)
    if [ "$fishpromptvar" != "$tempfish" ]
        set fishpromptvar $tempfish
        printf $tempfish
        printf "\n"
    end

    if [ $last_status -eq 0 ]
        echo -ne "\033[00;01m"
    else
        echo -ne "\033[01;31m"
    end
    printf "> "
    echo -ne "\033[00m"
end

# at the command prompt, type "pf" to show the two-line statusline
function pf
                set fishpromptvar ""
end

# some aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias lha='ls -lha'
alias df="df -h"
alias du="du -h --apparent-size"

# hide the default fish greeting
set fish_greeting ""

# set default title
set fishtitlevar "fish"

# sets the title of the shell
function fish_title
    echo $fishtitlevar
end

# at the command prompt, run "title foobar" to set the shell title to foobar
function title
    set fishtitlevar $argv[1]
end

alias pkexec='pkexec env DISPLAY="$DISPLAY" XAUTHORITY="$XAUTHORITY" DBUS_SESSION_BUS_ADDRESS="$DBUS_SESSION_BUS_ADDRESS"'
alias nano='nano -AWcimwxlT4'
alias gitk='gitk --all --date-order'
alias clip='xclip -selection clipboard'

if [ -f "$HOME/.autosudorc" ]
	. "$HOME/.autosudorc"
end
